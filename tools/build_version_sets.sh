#!/usr/bin/env bash

cp=$(basename `pwd`)

mkdir -p content/version_sets

echo "Building version sets"

for part in test prod dev ; do
    cat > content/version_sets/${cp}-${part}.yaml <<EOF
---
Apply: true
Components:
  - Type: ContentPackage
    Version: v0.2.0
    Name: bankn-${cp}-${part}
    ActualVersion: ''
  - Type: ContentPackage
    Version: stable
    Name: bankn-${cp}
Description: Version of bankn-${cp}-${part} to apply
Documentation: >
  Version of bankn-${cp}-${part} to apply
Id: ${cp}-${part}
Meta:
  color: olive
  icon: money
EOF

done
